package com.example.msi_ge60.coachyouremotions;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by bona on 26/03/18.
 */

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void start(View view){
        Intent intentMain = new Intent(MainActivity.this, MainMenu.class);
        if(view.getId() == R.id.monEspaceMenu){
            intentMain.putExtra("key", 2);
        }
        if(view.getId() == R.id.exercerMenu){
            intentMain.putExtra("key", 1);
        }
        if(view.getId() == R.id.ressourceMenu){
            intentMain.putExtra("key", 0);
        }

        MainActivity.this.startActivity(intentMain);
    }
}
