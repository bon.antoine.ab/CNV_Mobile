package com.example.msi_ge60.coachyouremotions;

/**
 * Created by msi-GE60 on 23/03/2018.
 */

import android.app.Activity;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by msi-GE60 on 22/03/2018.
 */

public class BesoinActivity extends ExpandableListActivity {

    private Context mContext;
    private Activity mActivity;

    private TextView mTextView;
    private String mJSONURLString = "http://51.255.131.197/cye/besoin";

    String[][] myContent;
    List<String> nomCat = new ArrayList<>();

    private ArrayList<String> parentItems = new ArrayList<String>();
    private ArrayList<Object> childItems = new ArrayList<Object>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sentiments);

        TextView infoText = (TextView)findViewById(R.id.infoText);
        infoText.setText("Besoins");
        infoText.setBackgroundColor(getResources().getColor(R.color.blueBackground));
        TextView sp = (TextView)findViewById(R.id.sp);
        sp.setBackgroundColor(getResources().getColor(R.color.blueBackground));

        // Get the application context
        mContext = getApplicationContext();
        mActivity = BesoinActivity.this;

        if(!CheckConnexion.haveNetworkConnection(this.mContext)){
            TextView errorText = (TextView)findViewById(R.id.errorText);
            errorText.setText("Vérifiez votre connexion a internet ou au serveur");
            LinearLayout layout = new LinearLayout(BesoinActivity.this);
            if(layout.getParent()!=null){
                ((ViewGroup)layout.getParent()).removeAllViews();
            }
            layout.removeAllViewsInLayout();
            LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            errorText.setVisibility(View.VISIBLE);
            errorText.setBackgroundColor(getResources().getColor(R.color.blueBackground));
        }

        myContent = new String[999][3];//SERA MODIFIE APRES AVEC UNE NOUVELLE REQUETE

        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(mJSONURLString,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        String tmp = "";
                        // Process the JSON
                        try {
                            // Loop through the array elements
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object
                                JSONObject student = response.getJSONObject(i);

                                // Get the current student (json object) data

                                String categorie = URLDecoder.decode(URLEncoder.encode(student.getString("categorie"), "iso8859-1"), "UTF-8");
                                //int id = student.getInt("id");
                                String nom = URLDecoder.decode(URLEncoder.encode(student.getString("nom"), "iso8859-1"), "UTF-8");
                                if (i == 0) {
                                    tmp = categorie;
                                    nomCat.add(tmp);
                                }
                                if (!tmp.equals(categorie)) {
                                    tmp = categorie;
                                    nomCat.add(tmp);
                                }


                                // Display the formatted json data in text view


                                myContent[i][0] = categorie;
                                myContent[i][1] = nom;

                            }

                            //POUR L'ACCORDEON !!!!!!!!!!!!!!

                            ExpandableListView expandableList = getExpandableListView();

                            expandableList.setDividerHeight(2);
                            expandableList.setGroupIndicator(null);
                            expandableList.setClickable(true);

                            setGroupParents();


                            MyExpandableAdapter adapter = new MyExpandableAdapter(parentItems, childItems);

                            adapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE), mActivity);
                            expandableList.setAdapter(adapter);
                            //expandableList.setOnChildClickListener();

                            //FIN DE L'INIT DE L'ACCORDEON !!!!!!!!!!!!!!!!
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                    }
                });

        // Add JsonArrayRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);
    }


    //FIN DU GET SUR LE SERVEUR

    //DEBUT DU STOCKAGE DES DONNEES DANS L'ACCORDEON

    public void setGroupParents() {
        for(String g: nomCat){
            parentItems.add(g);
            setChildData(g);
        }
    }

    public void setChildData(String nomCateg) {
        String tmp[];

        ArrayList<String> child = new ArrayList<String>();
        for(int i = 0; i < myContent.length && myContent[i][1] != null; i++){
            if(myContent[i][0].equals(nomCateg)){
                child.add(myContent[i][1]+"\n");
            }
        }
        childItems.add(child);

    }

}

