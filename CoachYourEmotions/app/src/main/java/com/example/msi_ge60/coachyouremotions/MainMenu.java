package com.example.msi_ge60.coachyouremotions;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class MainMenu extends AppCompatActivity {

    private Context mContext;
    private Activity mActivity;

    private String mJSONURLString = "http://51.255.131.197/cye/exercice";

    private List<String> myContent = new ArrayList<String>();
    private List<String> consigneGlobale = new ArrayList<String>();

    private List<String> loginList = new ArrayList<String>();
    private List<String> passwordList = new ArrayList<String>();

    private String loginText = "";
    private String passwordText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //TabInit

        mContext = this.getApplicationContext();

        if(!CheckConnexion.haveNetworkConnection(this.mContext)){
            TextView errorText = (TextView)findViewById(R.id.exercerText);
            errorText.setText("Vérifiez votre connexion a internet");
            LinearLayout layout = new LinearLayout(this);
            if(layout.getParent()!=null){
                ((ViewGroup)layout.getParent()).removeAllViews();
            }
            layout.removeAllViewsInLayout();
            LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            errorText.setBackgroundColor(getResources().getColor(R.color.orangeBouton));
        }

        TabHost host = (TabHost)findViewById(R.id.tabHost);
        host.setup();

        //Tab 1
        TabHost.TabSpec spec = host.newTabSpec("Tab One");
        spec.setContent(R.id.ressources);
        spec.setIndicator("Ressources");
        host.addTab(spec);

        //Tab 2
        spec = host.newTabSpec("Tab Two");
        spec.setContent(R.id.sexercer);
        spec.setIndicator("S'exercer");
        mActivity = MainMenu.this;
        setButtonExercice();

        host.addTab(spec);

        //Tab 3
        spec = host.newTabSpec("Tab Three");
        spec.setContent(R.id.monEspace);
        spec.setIndicator("Mon Espace");
        host.addTab(spec);

        //RessourcesInit

        final Button aideMemoireButton = (Button) findViewById(R.id.aideMemoireButton);
        final Button sentimentsButton = (Button) findViewById(R.id.sentimentsButton);
        final Button besoinsButton = (Button) findViewById(R.id.besoinsButton);

        View.OnClickListener handler = new View.OnClickListener(){

            public void onClick(View v) {

                if(v==aideMemoireButton){
                    Intent intentMain = new Intent(MainMenu.this, AideMemoireActivity.class);
                    MainMenu.this.startActivity(intentMain);
                }

                if(v==sentimentsButton){
                    Intent intentMain = new Intent(MainMenu.this, sentimentsActivity.class);
                    MainMenu.this.startActivity(intentMain);
                }

                if(v==besoinsButton){
                    Intent intentMain = new Intent(MainMenu.this, BesoinActivity.class);
                    MainMenu.this.startActivity(intentMain);
                }


            }
        };

        aideMemoireButton.setOnClickListener(handler);
        sentimentsButton.setOnClickListener(handler);
        besoinsButton.setOnClickListener(handler);

        TabHost tabHost = (TabHost)findViewById(R.id.tabHost);
        Bundle extras = this.getIntent().getExtras();
        tabHost.setCurrentTab(extras.getInt("key"));

        Button con = (Button)findViewById(R.id.connect);
        con.setEnabled(false);

    }

    public void setButtonExercice(){
        // Get the application context
        mContext = getApplicationContext();
        mActivity = MainMenu.this;

        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(mJSONURLString,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        String tmp = "";
                        // Process the JSON
                        try {
                            // Loop through the array elements
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object
                                JSONObject student = response.getJSONObject(i);

                                // Get the current student (json object) data
                                String cg = URLDecoder.decode(URLEncoder.encode(student.getString("cg"), "iso8859-1"), "UTF-8");
                                String nom = URLDecoder.decode(URLEncoder.encode(student.getString("nom"), "iso8859-1"), "UTF-8");



                                // Display the formatted json data in text view
                                myContent.add(nom);
                                consigneGlobale.add(cg);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        for(int i = 0; i < myContent.size(); i++) {
                            Button b = new Button(mActivity);
                            b.setBackgroundColor(getResources().getColor(R.color.orangeBouton));

                            //FrameLayout layout = (FrameLayout) findViewById(R.layout.);
                            final int tmp2 = i;
                            b.setOnClickListener(value -> intentB(tmp2));
                            b.setText(myContent.get(i));
                            TextView space = new TextView(mActivity);
                            space.setText("");
                            space.setBackgroundColor(getResources().getColor(R.color.orangeBackground));
                            LinearLayout act = (LinearLayout) findViewById(R.id.sexercer);
                            LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            act.addView(space);
                            act.addView(b, layoutParam);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(CheckConnexion.haveNetworkConnection(mContext)) {
                            TextView errorText = (TextView) findViewById(R.id.exercerText);
                            errorText.setText("Nous rencontrons des problèmes de connexion: vérifiez que le serveur fonctionne et que la BDD est active, pour plus d'informations contacter l'administrateur");
                        }
                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                    }
                });

        // Add JsonArrayRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);
    }

    void intentB(int id){
        Intent intentMain = new Intent(MainMenu.this, ExoActivity.class);
        intentMain.putExtra("id", id);
        intentMain.putExtra("cg", consigneGlobale.get(id));
        MainMenu.this.startActivity(intentMain);
    }

    void connect(View view){
        CheckLogin();
        EditText loginInput = (EditText)findViewById(R.id.login);
        EditText passwordInput = (EditText)findViewById(R.id.password);
        loginText = loginInput.getText().toString();
        passwordText = passwordInput.getText().toString();

    }

    public void CheckLogin(){
        // Get the application context
        mContext = getApplicationContext();
        mActivity = MainMenu.this;

        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(mJSONURLString,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        String tmp = "";
                        // Process the JSON
                        try {
                            System.out.println("début");
                            // Loop through the array elements
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object
                                JSONObject loginBDD = response.getJSONObject(i);

                                // Get the current loginBDD (json object) data
                                String login = URLDecoder.decode(URLEncoder.encode(loginBDD.getString("name"), "iso8859-1"), "UTF-8");
                                String password =  URLDecoder.decode(URLEncoder.encode(loginBDD.getString("alias"), "iso8859-1"), "UTF-8");

                                loginList.add(login);
                                passwordList.add(password);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        System.out.println(loginList);
                        if(checkConnectSuccess()){
                            System.out.println("Successssssss");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                    }
                });

        // Add JsonArrayRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);
    }

    public boolean checkConnectSuccess(){
        boolean tmp = false;
        boolean tmp2 = false;
        for(String l: loginList){
            if(l.equals(loginText)){
                tmp = true;
            }else{
                tmp = false;
            }
            for(String p: passwordList){
                System.out.println(p);
                if(p.equals(passwordText)){
                    tmp2 = true;
                }else{
                    tmp2 = false;
                }
                if(tmp & tmp2){
                    return true;
                }
            }
        }
        return false;
    }

    public void inscriptions(View view){
        Intent intentMain = new Intent(MainMenu.this, Inscription.class);
        MainMenu.this.startActivity(intentMain);
    }

}



