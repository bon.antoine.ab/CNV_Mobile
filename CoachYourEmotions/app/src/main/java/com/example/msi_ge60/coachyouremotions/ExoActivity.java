package com.example.msi_ge60.coachyouremotions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by msi-GE60 on 23/03/2018.
 */

public class ExoActivity extends AppCompatActivity {

    int exoID = 0;
    String cg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exo_layout);
        Bundle extras = getIntent().getExtras();
        TextView txt  = (TextView)findViewById(R.id.indication);
        exoID = Integer.valueOf(extras.get("id").toString());
        cg = extras.get("cg").toString();
        TextView indication = (TextView)findViewById(R.id.indication);
        indication.setText(cg);
    }

    public void start(View view){
        Intent intentMain = new Intent(ExoActivity.this, Question.class);
        intentMain.putExtra("exoID", exoID);
        ExoActivity.this.startActivity(intentMain);
    }
}
