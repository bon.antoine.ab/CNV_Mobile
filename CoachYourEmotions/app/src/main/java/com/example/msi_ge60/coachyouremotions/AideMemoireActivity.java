package com.example.msi_ge60.coachyouremotions;

import java.util.ArrayList;

import android.app.ExpandableListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class AideMemoireActivity extends ExpandableListActivity{

    private ArrayList<String> parentItems = new ArrayList<String>();
    private ArrayList<Object> childItems = new ArrayList<Object>();

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // this is not really  necessary as ExpandableListActivity contains an ExpandableList
        setContentView(R.layout.aide_memoire);

        ExpandableListView expandableList = getExpandableListView();

        expandableList.setDividerHeight(2);
        expandableList.setGroupIndicator(null);
        expandableList.setClickable(true);

        setGroupParents();
        setChildData();

        MyExpandableAdapter adapter = new MyExpandableAdapter(parentItems, childItems);

        adapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE), this);
        expandableList.setAdapter(adapter);
        expandableList.setOnChildClickListener(this);

        TextView ressourcesText = (TextView)findViewById(R.id.ressourcesText);
        ressourcesText.setText(R.string.ressource_Text);
    }

    public void setGroupParents() {
        parentItems.add("L'expression de l'observation");
        parentItems.add("L'expression du sentiment");
        parentItems.add("L'expression du besoin");
        parentItems.add("L'expression de la demande");
    }

    public void setChildData() {
        String tmp[];
        // Android
        ArrayList<String> child = new ArrayList<String>();
        tmp = getString(R.string.choix_1).split("\n");
        for(int i = 0; i < tmp.length; i++){
            child.add(tmp[i]);
        }
        childItems.add(child);

        // Core Java
        child = new ArrayList<String>();
        tmp = getString(R.string.choix_2).split("\n");
        for(int i = 0; i < tmp.length; i++){
            child.add(tmp[i]);
        }
        childItems.add(child);

        // Desktop Java
        child = new ArrayList<String>();
        tmp = getString(R.string.choix_3).split("\n");
        for(int i = 0; i < tmp.length; i++){
            child.add(tmp[i]);
        }
        childItems.add(child);

        // Enterprise Java
        child = new ArrayList<String>();
        tmp = getString(R.string.choix_4).split("\n");
        for(int i = 0; i < tmp.length; i++){
            child.add(tmp[i]);
        }
        childItems.add(child);
    }

}