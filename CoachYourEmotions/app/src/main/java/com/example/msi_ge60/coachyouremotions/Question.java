package com.example.msi_ge60.coachyouremotions;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by msi-GE60 on 23/03/2018.
 */

public class Question extends AppCompatActivity{

    private int exoID = 0;

    private Context mContext;
    private Activity mActivity;
    int j = 0;
    private String[][] myContent = new String[999][4];
    int nbQuestions = 0;
    int nbPoints = 0;


    private String mJSONURLString = "http://51.255.131.197/cye/question/exoid/";

    List<String> questionList = new ArrayList<String>();
    List<String> reponseList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_layout);
        Bundle extras = getIntent().getExtras();
        exoID = Integer.valueOf(extras.get("exoID").toString());

        mJSONURLString += (exoID+1);//CAR EN BDD, LES INDEX COMMENCENT A 1 !!!;
        System.out.println(mJSONURLString);

        getQuestion();

    }

    public void getQuestion(){
        // Get the application context
        mContext = getApplicationContext();
        mActivity = Question.this;

        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(mJSONURLString,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Do something with response
                        //mTextView.setText(response.toString());
                        String tmp = "";
                        // Process the JSON
                        try {
                            // Loop through the array elements
                            System.out.println("test1 : " + response.length());
                            for (int i = 0; i < response.length(); i++) {
                                // Get current json object
                                System.out.println("test2");
                                JSONObject student = response.getJSONObject(i);

                                // Get the current student (json object) data
                                String question = URLDecoder.decode(URLEncoder.encode(student.getString("question"), "iso8859-1"), "UTF-8");
                                String correctionOui = URLDecoder.decode(URLEncoder.encode(student.getString("correctionOui"), "iso8859-1"), "UTF-8");
                                String correctionNon = URLDecoder.decode(URLEncoder.encode(student.getString("correctionNon"), "iso8859-1"), "UTF-8");
                                String repAtt = student.getString("reponseAttendue");

                                // Display the formatted json data in text view
                                myContent[i][0] = question;
                                myContent[i][1] = correctionOui;
                                myContent[i][2] = correctionNon;
                                myContent[i][3] = repAtt;
                                System.out.println(question + " "+ correctionOui + " fklezjfijelk");
                                TextView quest = (TextView)findViewById(R.id.question);
                                quest.append(question + " " + correctionNon + " fmlkjsdmlfjds\n");
                            }

                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }

                        if(myContent[j][0] == null){

                        }

                        TextView quest = (TextView)findViewById(R.id.question);
                        quest.setText(myContent[0][0]);
                        Button ouiB = (Button)findViewById(R.id.oui);
                        Button nonB = (Button)findViewById(R.id.non);
                        Button suiv = (Button)findViewById(R.id.suivant);
                        ouiB.setBackgroundColor(getResources().getColor(R.color.orangeBouton));
                        nonB.setBackgroundColor(getResources().getColor(R.color.orangeBouton));
                        suiv.setBackgroundColor(getResources().getColor(R.color.orangeBouton));
                        ouiB.setOnClickListener(value -> button("oui"));
                        nonB.setOnClickListener(value -> button("non"));
                        suiv.setOnClickListener(value -> {
                            j++;
                            ouiB.setTextColor(Color.BLACK);
                            nonB.setTextColor(Color.BLACK);
                            ouiB.setEnabled(true);
                            nonB.setEnabled(true);
                            quest.setText(myContent[j][0]);
                            TextView reponse = (TextView)findViewById(R.id.reponse);
                            reponse.setText("");
                            if(myContent[j][0] == null){
                                quest.setText("Félicitations d’avoir pris quelques minutes pour vous exercer à la Communication NonViolente et de confronter votre vision à la notre. N’hésitez pas à continuer avec les autres exercices à votre disposition !");
                                ouiB.setVisibility(View.GONE);
                                nonB.setVisibility(View.GONE);
                                suiv.setText("Quitter");
                                suiv.setOnClickListener(e->{
                                    mActivity.finish();
                                });
                            }
                        });
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                    }
                });

        // Add JsonArrayRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);
    }

    private void button(String rep){
        TextView reponse = (TextView)findViewById(R.id.reponse);
        Button ouiB = (Button)findViewById(R.id.oui);
        Button nonB = (Button)findViewById(R.id.non);
        if(rep.equals("oui")){
            reponse.setText(myContent[j][1]);
            if(myContent[j][3].equals("oui")){
                ouiB.setTextColor(Color.GREEN);
            }
            else ouiB.setTextColor(Color.RED);
        }
        else {
            reponse.setText(myContent[j][2]);
            if(myContent[j][3].equals("non")){
                nonB.setTextColor(Color.GREEN);
            }
            else nonB.setTextColor(Color.RED);
        }
        ouiB.setEnabled(false);
        nonB.setEnabled(false);
    }

}
